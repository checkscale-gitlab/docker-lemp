<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style>
    div {
      margin-bottom:64px;
      display:table;
      width:100%;
    }
    p.text {
      font-size:32px;
    }
  </style>
</head>
<body style="width:80%;margin:0 auto; text-align:center">

<div>
      <?php
$servername   = "db";
$username = "root";
$password = "password";

// Create connection
$conn = new mysqli($servername, $username, $password);
// Check connection
if ($conn->connect_error) {
   echo "<p style='color:red; font-weight:bold;'>Database connection error</p>";
} else {
  echo "<p style='color:green;font-weight:normal;'>Successfully connected to <database></database> with username: <strong style='color:black'> $username</strong>, password: <strong style='color:black'>$password</strong>, and host: <strong style='color:black'>$servername</strong></p>";
}
?>
</div>
<div>
  <img src="https://i.imgur.com/YJxQJYr.png" style="margin:0 auto; width:100%">
</div>
<div>
  <p class="text">Find out more on the <a href="https://gitlab.com/paveljame/docker-lemp">GitLab</a></p>
</div>
</body>

</html>
<?php phpinfo(); ?>