FROM php:7.3-fpm-alpine
# You can add options for image instalation. See readme for more info
ARG wordpress=no

# install some the PHP extensions 
RUN set -ex; \
	\
	apk add --no-cache --virtual .build-deps \
		libzip-dev \
		$PHPIZE_DEPS \
		imagemagick-dev \
		libjpeg-turbo-dev \
		libpng-dev \
	; \
	\
	docker-php-ext-configure gd --with-png-dir=/usr --with-jpeg-dir=/usr; \
	docker-php-ext-install -j "$(nproc)" \
		gd \
		pdo \
		zip  \
		exif  \
		bcmath \
		mysqli  \
		opcache  \
		pdo_mysql \
	; \
	\
	runDeps="$( \
		scanelf --needed --nobanner --format '%n#p' --recursive /usr/local/lib/php/extensions \
			| tr ',' '\n' \
			| sort -u \
			| awk 'system("[ -e /usr/local/lib/" $1 " ]") == 0 { next } { print "so:" $1 }' \
	)"; \
	apk add --virtual .wordpress-phpexts-rundeps $runDeps; \
	apk add --no-cache supervisor; \
	apk del .build-deps

# set recommended PHP.ini settings
# see https://secure.php.net/manual/en/opcache.installation.php
RUN { \
		echo 'opcache.memory_consumption=128'; \
		echo 'opcache.interned_strings_buffer=8'; \
		echo 'opcache.max_accelerated_files=4000'; \
		echo 'opcache.revalidate_freq=2'; \
		echo 'opcache.fast_shutdown=1'; \
	} > /usr/local/etc/php/conf.d/opcache-recommended.ini

# Section with special options
RUN if [ "$OPTIONS" = "wordpress" ] ; then \
		set -ex; \
			apk add --no-cache --virtual \
				libmemcached-dev \
				curl \
				mysql-client \
				less \
				zlib \
			; \
			docker-php-ext-enable pdo pdo_mysql mysqli gd \
			; \	
		curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar ; \
		chmod +x wp-cli.phar ; \
		mv wp-cli.phar /usr/local/bin/wp ; \
fi

# Create cron log files 
RUN mkdir -p /var/log/cron \
 && touch /var/log/cron/cron.log \
 && touch /var/log/cron/crontab.log 

# Copy our cronjob list
COPY ./cron/crontab.txt /cronjob.txt
RUN	crontab /cronjob.txt

# Copy our supervisor config file
COPY ./cron/supervisord.conf /etc/supervisor/supervisord.conf

# Let's go
CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/supervisord.conf"]