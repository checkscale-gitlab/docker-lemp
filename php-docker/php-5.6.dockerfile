FROM php:5.6-fpm-alpine 
# You can add options for image instalation. See readme for more info
ARG wordpress=no

# install some the PHP extensions 
RUN set -ex; \
	\
	apk add --no-cache --virtual .build-deps \
		libzip-dev \
		$PHPIZE_DEPS \
		imagemagick-dev \
		libjpeg-turbo-dev \
		libpng-dev \
	; \
	\
	docker-php-ext-configure gd --with-png-dir=/usr --with-jpeg-dir=/usr; \
	docker-php-ext-install -j "$(nproc)" \
		gd \
		pdo \
		zip  \
		exif  \
		bcmath \
		mysqli  \
		opcache  \
		pdo_mysql \
	; \
	\
	runDeps="$( \
		scanelf --needed --nobanner --format '%n#p' --recursive /usr/local/lib/php/extensions \
			| tr ',' '\n' \
			| sort -u \
			| awk 'system("[ -e /usr/local/lib/" $1 " ]") == 0 { next } { print "so:" $1 }' \
	)"; \
	apk add --virtual .wordpress-phpexts-rundeps $runDeps; \
	apk del .build-deps

# Section with special options
RUN if [ "$wordpress" = "yes" ] ; then \
		set -ex; \
			apk add --no-cache --virtual \
				libmemcached-dev \
				curl \
				mysql-client \
				less \
				zlib \
			; \
			docker-php-ext-enable pdo pdo_mysql mysqli gd \
			; \	
		curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar ; \
		chmod +x wp-cli.phar ; \
		mv wp-cli.phar /usr/local/bin/wp ; \
fi

CMD ["php-fpm"]